import express from 'express'
// import AssistantV2 from 'watson-developer-cloud/assistant/v2'
import watson from 'watson-developer-cloud'

const router = express.Router()
 
function WebhookProcessing(req, res) {
    return { message: 'hello' };
}

var session_id;
router.get('/test', function (req, res) {
    console.info(`\n\n>>>>>>> S E R V E R   H I T <<<<<<<`);
    const service = new watson.AssistantV2({
        iam_apikey:  process.env.IAM_API_KEY,
        version: '2019-02-28',
        url: process.env.WATSON_URL
    });
    service.createSession({
        assistant_id: process.env.ASSISTANT_ID,
      }, function(err, response) {
        if (err) {
          console.error(err);
        } else{
          console.log(JSON.stringify(response, null, 2));
          session_id = response.session_id
          service.message({
            assistant_id: process.env.ASSISTANT_ID,
            session_id: session_id,
            input: {
                'message_type': 'text',
                'text': 'translate slave'}
            }, function(err, response) {
            if (err)
                console.log('error:', err);
            else
                console.log(JSON.stringify(response, null, 2));
            });
           }
      });
});

export default router
