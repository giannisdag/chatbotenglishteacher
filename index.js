import 'dotenv/config'
import express from 'express'
import bodyParser from 'body-parser'
import dialogFlow from './dialogflow'
import watson from './watson'
import wit from './wit'
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/dialogflow', dialogFlow)
app.use('/wit', wit)
// app.use('/watson', watson)

app.listen(3000, function () {
    console.info(`Webhook listening on port 3000!`)
});