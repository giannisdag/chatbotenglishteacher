import {Wit, log, interactive} from 'node-wit'
import express from 'express'
import {getRandomInt, firstEntity} from './helpers/helperFunctions'
import vocabulary from './data/vocabulary.json'
import questions from './data/questions.json'
const router = express.Router()

var WordInGreek;
const firstEntityValue = (entities, entity) => {
    const val = entities && entities['intent'] &&
      Array.isArray(entities['intent']) &&
      entities['intent'].length > 0 &&
      entities['intent'][0].value
    ;
    if (entities['intent']!==undefined) {
        // console.log(entities['intent'])
    }
    if (val===entity) {
      return val;
    }
    return null;
  };

const askQuestion = (examination) => {
    let num = getRandomInt(20);
    WordInGreek = vocabulary.entry[num].translation;
    let quest_num = getRandomInt(2);
    let question = questions[quest_num];
    question = question.replace("{word}",WordInGreek);
    console.log(question)
};

const handleMessage = ({entities}) => {
    // console.log(entities)    
    const greetings = firstEntityValue(entities, 'greetings');
    const translation = firstEntityValue(entities, 'translation');
    // user translates a word from greek to english 
    const reply_translation = firstEntityValue(entities, 'reply_translation');
    const examination = firstEntityValue(entities, 'examination');
    if (examination) {
      askQuestion(examination);
    } else if (reply_translation) {
        console.log("🤖 > Για να δούμε αν απάντησες σωστά");
        if(entities.dictionary_en!=undefined) {
          let en_word = entities.dictionary_en[0].value;
          console.log(en_word);
          const translatedWord = vocabulary.entry.find(item => item.term === en_word);
          if (translatedWord !== undefined && (translatedWord.translation==WordInGreek)) {
            console.log(`🤖 > Μπράβο σωστά, η λέξη ${WordInGreek} σημαίνει ${translatedWord.term}`);          
          } else {
            console.log("🤖 > Δυστυχώς έκανες λάθος");          
          }
        }
    } else if (greetings) {
        console.log("🤖 > Γειά, σου θέλεις να σε εξετάσω στο λεξιλόγιο;");
    } else {
      console.log("🤖 > Χμμμ, δεν κατάλαβα, μπορείς να επαναλάβεις;");
    }
  };
  
router.get('/test', function (req, res) {
    const client = new Wit({
        accessToken: process.env.ACCESS_TOKEN_WIT,
        logger: new log.Logger(log.DEBUG) // optional
      });
      interactive(client,handleMessage);
    //   return client.message(question, {})
    //   .then(({entities}) => {
    //     const intent = firstEntity(entities, 'intent');
    //     if (!intent) {
    //       // use app data, or a previous context to decide how to fallback
    //       return;
    //     }
    //     switch (intent.value) {
    //       case 'translation':
    //         console.log('🤖 > Okay, lets reply to question');

    //         break;
    //       case 'reply_translation':
    //         console.log('🤖 > Okay, lets check if the reply is correct');
    //         break;
    //       default:
    //         console.log(`🤖  ${intent.value}`);
    //         break;
    // }
    //   })
    //   .catch(console.error); 
    //   console.log(client.message('Πως λέγεται ο δούλος;'));
});


export default router

