// 'use strict';

import express from 'express'
import {getRandomInt} from './helpers/helperFunctions'
import {WebhookClient} from 'dialogflow-fulfillment'
import vocabulary from './data/vocabulary.json'
const router = express.Router()

function welcome (agent) {
    agent.add(`Welcome to Express.JS webhook!`);
}

function fallback (agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
}

function testFunctionHandler(agent) {
    agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
    agent.add(new Card({
        title: `Title: this is a card title`,
        imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
        text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
        buttonText: 'This is a button',
        buttonUrl: 'https://assistant.google.com/'
      })
    );
    agent.add(new Suggestion(`Quick Reply`));
    agent.add(new Suggestion(`Suggestion`));
    agent.setContext({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
  }
 
 function directTranslationHandler(agent) {
   var num = getRandomInt(20);
   var en_word = vocabulary.entry[num].term;
  //  const en_word = 'slave';
   agent.add(`Ok what does ${en_word} means in Greek?`);
   
   agent.context.set({name:'english_word_test', lifespan: 3, parameters: {en_word:en_word}})
 }
 
  function directTranslationFollowHandler(agent) {
    // const req = agent.request_.body;// request.body;
    // const requestContexts = req.queryResult.outputContexts;
    // console.log(requestContexts);
    // var con = requestContexts[0]; //.find( item => item.name== 'projects/englishteacher-ae254/agent/sessions/a5ccc559-8865-964a-ec53-2226155ce287/contexts/english_word_test');
    
    var cont = agent.context.get('english_word_test');
    // const word = agent.parameters['word'];
    const word = agent.parameters.word;
    const translatedWord = vocabulary.entry.find( item => item.translation === word);
    if (translatedWord !== undefined && (translatedWord.term==cont.parameters.en_word)) {
      agent.add(`You are correct, ${word} means ${translatedWord.term}`);
    } else {
      agent.add('No error, what should we do?');
    }     
  }
 
  function translateHandler(agent) {
      const word = agent.parameters.word;
   //   console.log(word);
      const translatedWord = vocabulary.entry.find( item => item.term === word);
      console.log(translatedWord);
      if (translatedWord !== undefined) {
          agent.add(`The ${word} means ${translatedWord.translation} in Greek`);
      } else {
          agent.add('I will ask google');
      }
  }

function WebhookProcessing(req, res) {
    const agent = new WebhookClient({request: req, response: res});
    console.info(`agent set`);

    let intentMap = new Map();
    intentMap.set('Default Welcome Intent', welcome);
    intentMap.set('Default Fallback Intent', fallback);
    intentMap.set('what about', testFunctionHandler);
    intentMap.set('directTranslation', directTranslationHandler);
    intentMap.set('directTranslation - follow', directTranslationFollowHandler);
    intentMap.set('reverseTranslation', translateHandler); 
    agent.handleRequest(intentMap);
}

// Webhook
router.post('/test', function (req, res) {
    console.info(`\n\n>>>>>>> S E R V E R   H I T <<<<<<<`);
    WebhookProcessing(req, res);
});

export default router