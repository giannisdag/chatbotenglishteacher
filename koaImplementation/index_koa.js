const Koa = require('koa');
const router = require('./routes');
const app = new Koa();
const https = require('https');
const http = require('http');
const bodyParser = require('koa-bodyparser');
app.on('error', (err, ctx) => {
    console.log(err);
 });
app.use(bodyParser({
    enableTypes: ['json'],
    jsonLimit: '5mb',
    strict: true,
    onerror: function (err, ctx) {
      ctx.throw('body parse error', 422)
    }
  }));
   

// logger

app.use(async (ctx, next) => {
    await next();
    const rt = ctx.response.get('X-Response-Time');
    console.log(`${ctx.method} ${ctx.url} - ${rt}`);
  });
  
  // x-response-time
  
  app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
  });
  

app
  .use(router.routes())
  .use(router.allowedMethods());
http.createServer(app.callback()).listen(3000);
