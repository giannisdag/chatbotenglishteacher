const Router = require('koa-router');
const router = new Router();
let vocabulary = require('../data/vocabulary.json');
// router.prefix('/v1')

router.get('/', (ctx, next) => {
    ctx.body = 'Hello World koa';
});
router.post('/test', (ctx, next) => {
    // See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
// const functions = require('firebase-functions');
console.log(ctx.req.headers);
// const { WebhookClient } = require('dialogflow-webhook');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
 
// process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
// exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ 
    request: ctx.req,
    response: ctx.res
   });
  console.info(`agent set`);

  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function welcome(agent) {
    agent.add(`Welcome to my agent now man!`);
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }

  // // Uncomment and edit to make your own intent handler
  // // uncomment `intentMap.set('your intent name here', yourFunctionHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
   function testFunctionHandler(agent) {
     agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
     agent.add(new Card({
         title: `Title: this is a card title`,
         imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
         text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
         buttonText: 'This is a button',
         buttonUrl: 'https://assistant.google.com/'
       })
     );
     agent.add(new Suggestion(`Quick Reply`));
     agent.add(new Suggestion(`Suggestion`));
     agent.context.set({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
   }
  
  function directTranslationHandler(agent) {
    const en_word = 'console';
    console.log(en_word);
    agent.add(`Ok what does ${en_word} means in Greek?`);
    agent.context.set({name:'english_word_test', lifespan: 2, parameters: {en_word:en_word}})
    //  agent.setContext({name:'english_word_test', lifespan: 2, parameters: {en_word:en_word}})
  }
  
   function directTranslationFollowHandler(agent) {
//     var con = agent.context.get('projects/englishteacher-ae254/agent/sessions/a5ccc559-8865-964a-ec53-2226155ce287/contexts/english_word_test');
     const req = request.body;
//     console.log(req);
     const requestContexts = req.queryResult.outputContexts;
     console.log(requestContexts);
     var con = requestContexts[0]; //.find( item => item.name== 'projects/englishteacher-ae254/agent/sessions/a5ccc559-8865-964a-ec53-2226155ce287/contexts/english_word_test');
  //   var con = { parameters: {en_word:'slave'}};
     const word = agent.parameters['word'];
     const translatedWord = vocabulary.entry.find( item => item.translation === word);
     if (translatedWord !== undefined && (translatedWord.term==con.parameters.en_word)) {
       agent.add(` You are correct, ${word} means ${translatedWord.term}`);
     } else {
       agent.add('No error, what should we do?');
     }     
   }
  
   function translateHandler(agent) {
       const word = agent.parameters['word'];
    //   console.log(word);
       const translatedWord = vocabulary.entry.find( item => item.term === word);
      console.log(translatedWord);
       if (translatedWord !== undefined) {
           agent.add(`The ${word} means ${translatedWord.translation} in Greek`);
       } else {
           agent.add('I will ask google');
       }
   }

  // // Uncomment and edit to make your own Google Assistant intent handler
  // // uncomment `intentMap.set('your intent name here', googleAssistantHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
  // function googleAssistantHandler(agent) {
  //   let conv = agent.conv(); // Get Actions on Google library conv instance
  //   conv.ask('Hello from the Actions on Google client library!') // Use Actions on Google library
  //   agent.add(conv); // Add Actions on Google library responses to your agent's response
  // }
  // // See https://github.com/dialogflow/dialogflow-fulfillment-nodejs/tree/master/samples/actions-on-google
  // // for a complete Dialogflow fulfillment library Actions on Google client library v2 integration sample

  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('what about', testFunctionHandler);
  intentMap.set('directTranslation', directTranslationHandler);
  intentMap.set('directTranslation - follow', directTranslationFollowHandler);
  intentMap.set('translate', translateHandler); 
  // intentMap.set('your intent name here', googleAssistantHandler);
  agent.handleRequest(intentMap);
// });

});

module.exports = router

