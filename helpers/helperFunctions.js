export function getRandomInt (max) {
        return Math.floor(Math.random() * Math.floor(max));
}    
export function firstEntity(entities, name) {
        return entities &&
          entities[name] &&
          Array.isArray(entities[name]) &&
          entities[name] &&
          entities[name][0];
}
